#!/usr/bin/env bash

cmd="'$*'"

if [[ $cmd =~ .*branch.*(-d|-D|--delete).* ]]; then 
	repo=$(basename `git rev-parse --show-toplevel`)
	if command -v figlet >/dev/null 2>&1; then
		figlet "$repo"
	else
		echo "########################################"
		echo $repo
		echo "########################################"
	fi
	echo "You are currently in the $repo repo trying to delete a branch. Double check this is what you want to do!!!!!!"
	read -p "Continue (y/n)?" choice
	case "$choice" in
	  y|Y ) echo '(╯°□°）╯︵ ┻━┻ Here we go!!!' && $(command -v git) "$@";;
	  n|N ) echo '┬─┬ノ( º _ ºノ) Few that was close';;
	  * ) echo 'No idea what you want ¯\_(ツ)_/¯ Please answer with "y" or "n"';;
	esac
else
	$(command -v git) "$@"
fi
